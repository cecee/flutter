import 'package:flutter/material.dart';
import 'package:flutter_change_screen/ScreenA.dart';
import 'package:flutter_change_screen/ScreenB.dart';
import 'package:flutter_change_screen/ScreenC.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: '/',
      routes:{
        '/':(context)=>ScreenA(),
        '/b':(context)=>ScreenB(),
        '/c':(context)=>ScreenC(),
      },
    );
  }
}


