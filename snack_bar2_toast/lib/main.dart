import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('SnackBar2 & Toast'),
          centerTitle: true,
        ),
        body: Center(
          child: Column(
            children: [
              MySnackBar(),
              SizedBox(
                height: 30,
                width: 200,
                child: Text(
                  '~~~~~~SizedBox~~~~~',
                  textAlign: TextAlign.center,
                ),
              ),
              FlatButton(
                  color: Colors.amber,
                  onPressed: () {
                    flutterToast();
                  },
                  child: Text('Test Toast'))
            ],
          ),
        ));
  }
}

class MySnackBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: RaisedButton(
        child: Text('Show me'),
        onPressed: () {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text(
              'snack bar2',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
            backgroundColor: Colors.teal,
            duration: Duration(milliseconds: 1000),
          ));
        },
      ),
    );
  }
}

void flutterToast() {
  Fluttertoast.showToast(
    msg: 'null',
    gravity: ToastGravity.BOTTOM,
    backgroundColor: Colors.amber,
    fontSize: 20,
    textColor: Colors.white,
    toastLength: Toast.LENGTH_SHORT,
  );
}
