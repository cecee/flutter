package com.cecee.meetchudemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.mesibo.api.Mesibo;
import com.mesibo.calls.MesiboAudioCallFragment;
import com.mesibo.calls.MesiboCall;
import com.mesibo.calls.MesiboCallConfig;
import com.mesibo.calls.MesiboVideoCallFragment;
import com.mesibo.messaging.MesiboUI;


public class MainActivity extends AppCompatActivity implements Mesibo.ConnectionListener, Mesibo.MessageListener, MesiboCall.MesiboCallListener {
    protected final String TAG = getClass().getSimpleName();

     class DemoUser {
        public String token;
        public String name;
        public String address;
        public long groupid;
        DemoUser(String token, String name, String address, long groupid) {
            this.token = token;
            this.name = name;
            this.address = address;
            this.groupid = groupid;
        }
    }

    //Refer to the Get-Started guide to create two users and their access tokens
    DemoUser mUser1 = new DemoUser("42786a8ee79de240ddbc8465a7d09ba82dd1faa7a9d8437c7e4415d01f", "M1", "m1", 0);
    DemoUser mUser2 = new DemoUser("f3f8950ccae4e34aa1b81737de4d44a513aae0e397ec3eaa15d020", "M2", "m2", 0);
    DemoUser mUser3 = new DemoUser("2d88b4bab8129f525e34d4c88d4eb171ba509ee697cfe3315d025", "M3", "m3", 0);
    DemoUser mUser4 = new DemoUser("", "DEMO GROUP", null, 102806);

    DemoUser mRemoteUser;
    Mesibo.UserProfile mProfile;
    Mesibo.ReadDbSession mReadSession;

    Button mLogoutButton;
    View mLoginButton1, mLoginButton2, mLoginButton3, mSendButton;
    //TextView mMessageStatus, mConnStatus;
    //EditText mMessage;

    View userLayout1, userLayout2, userLayout3, userLayout4;
    View mUiButton1, mAudioCallButton1, mVideoCallButton1;
    View mUiButton2, mAudioCallButton2, mVideoCallButton2;
    View mUiButton3, mAudioCallButton3, mVideoCallButton3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLogoutButton = (Button) findViewById(R.id.logout);
        mLoginButton1 = findViewById(R.id.login1);
        mLoginButton2 = findViewById(R.id.login2);
        mLoginButton3 = findViewById(R.id.login3);

        //mSendButton = findViewById(R.id.send);
        //mMessageStatus = findViewById(R.id.msgStatus);
        //mConnStatus = findViewById(R.id.connStatus);
        //mMessage = findViewById(R.id.message);

        mUiButton1 = findViewById(R.id.launchUI1);
        mAudioCallButton1 = findViewById(R.id.audioCall1);
        mVideoCallButton1 = findViewById(R.id.videoCall1);

        mUiButton2 = findViewById(R.id.launchUI2);
        mAudioCallButton2 = findViewById(R.id.audioCall2);
        mVideoCallButton2 = findViewById(R.id.videoCall2);

        mUiButton3 = findViewById(R.id.launchUI3);
        mAudioCallButton3 = findViewById(R.id.audioCall3);
        mVideoCallButton3 = findViewById(R.id.videoCall3);

        userLayout1 = findViewById(R.id.userLayout1);
        userLayout2 = findViewById(R.id.userLayout2);
        userLayout3 = findViewById(R.id.userLayout3);
        userLayout4 = findViewById(R.id.userLayout4);

        userLayout1.setVisibility(View.GONE);
        userLayout2.setVisibility(View.GONE);
        userLayout3.setVisibility(View.GONE);
        userLayout4.setVisibility(View.GONE);
        mLogoutButton.setVisibility(View.GONE);

        //mSendButton.setEnabled(false);
        //mUiButton.setEnabled(false);
        //mAudioCallButton.setEnabled(false);
        //mVideoCallButton.setEnabled(false);
    }

//    private void mesiboInit(DemoUser user, DemoUser remoteUser) {
    private void mesiboInit(DemoUser user) {
        Mesibo api = Mesibo.getInstance();
        api.init(getApplicationContext());

        Mesibo.addListener(this);
        Mesibo.setSecureConnection(true);
        Mesibo.setAccessToken(user.token);
        Mesibo.setDatabase("meetchudb", 0);
        Mesibo.start();
//        mRemoteUser = remoteUser;
//        mProfile = new Mesibo.UserProfile();
//        mProfile.address = remoteUser.address;
//        mProfile.name = remoteUser.name;
//        Mesibo.setUserProfile(mProfile, false);

        // disable login buttons
        mLoginButton1.setVisibility(View.GONE);
        mLoginButton2.setVisibility(View.GONE);
        mLoginButton3.setVisibility(View.GONE);
        mLogoutButton.setText("Logout - " + user.name);
        mLogoutButton.setVisibility(View.VISIBLE);

        // enable buttons
        //mSendButton.setEnabled(true);
        //mUiButton.setEnabled(true);
        //mAudioCallButton.setEnabled(true);
        //mVideoCallButton.setEnabled(true);

        MesiboCall.getInstance().init(getApplicationContext());
        MesiboCall.getInstance().setListener(this);


        //MesiboCall c = new MesiboCall();
        MesiboCallConfig config = MesiboCall.getInstance().getConfig();
        config.layout_video = R.layout.fragment_videocall_new;
        //config.layout_video_activity = R.layout.activity_video;

        // Read receipts are enabled only when App is set to be in foreground
//        Mesibo.setAppInForeground(this, 0, true);
//        mReadSession = new Mesibo.ReadDbSession(mRemoteUser.address, this);
//        mReadSession.enableReadReceipt(true);
//        mReadSession.read(100);
    }


    public void onLoginUser1(View view) {
        mesiboInit(mUser1);
        userLayout2.setVisibility(View.VISIBLE);
        userLayout3.setVisibility(View.VISIBLE);
        userLayout4.setVisibility(View.VISIBLE);
    }

    public void onLoginUser2(View view) {
        mesiboInit(mUser2);
        userLayout1.setVisibility(View.VISIBLE);
        userLayout3.setVisibility(View.VISIBLE);
        userLayout4.setVisibility(View.VISIBLE);
    }

    public void onLoginUser3(View view) {
        mesiboInit(mUser3);
        userLayout1.setVisibility(View.VISIBLE);
        userLayout2.setVisibility(View.VISIBLE);
        userLayout4.setVisibility(View.VISIBLE);
    }

//    public void onSendMessage(View view) {
//        Mesibo.MessageParams p = new Mesibo.MessageParams();
//        p.peer = mRemoteUser.address;
//        p.flag = Mesibo.FLAG_READRECEIPT | Mesibo.FLAG_DELIVERYRECEIPT;
//
//        Mesibo.sendMessage(p, Mesibo.random(), mMessage.getText().toString().trim());
//        mMessage.setText("");
//    }

    public void setRemoteProfile(DemoUser remoteUser) {
        mRemoteUser = remoteUser;
        mProfile = new Mesibo.UserProfile();
        mProfile.address = remoteUser.address;
        mProfile.name = remoteUser.name;
        mProfile.groupid = remoteUser.groupid;
        Mesibo.setUserProfile(mProfile, false);
    }

    public void setRemoteGroupProfile(DemoUser remoteUser) {
        mRemoteUser = remoteUser;
        mProfile = new Mesibo.UserProfile();
        mProfile.name = remoteUser.name;
        mProfile.groupid = remoteUser.groupid;
        Mesibo.setUserProfile(mProfile, false);
    }

    public void onLaunchMessagingUi1(View view) {
        setRemoteProfile(mUser1);
        MesiboUI.launchMessageView(this, mRemoteUser.address, 0);
    }
    public void onAudioCall1(View view) {
        setRemoteProfile(mUser1);
        MesiboCall.getInstance().call(this, 0, mProfile, false);
    }
    public void onVideoCall1(View view) {
        setRemoteProfile(mUser1);
        MesiboCall.getInstance().call(this, 0, mProfile, true);
    }

    public void onLaunchMessagingUi2(View view) {
        setRemoteProfile(mUser2);
        MesiboUI.launchMessageView(this, mRemoteUser.address, 0);
    }
    public void onAudioCall2(View view) {
        setRemoteProfile(mUser2);
        MesiboCall.getInstance().call(this, 0, mProfile, false);
    }
    public void onVideoCall2(View view) {
        setRemoteProfile(mUser2);
        MesiboCall.getInstance().call(this, 0, mProfile, true);
    }

    public void onLaunchMessagingUi3(View view) {
        setRemoteProfile(mUser3);
        MesiboUI.launchMessageView(this, mRemoteUser.address, 0);
    }
    public void onAudioCall3(View view) {
        setRemoteProfile(mUser3);
        MesiboCall.getInstance().call(this, 0, mProfile, false);
    }
    public void onVideoCall3(View view) {
        setRemoteProfile(mUser3);
        MesiboCall.getInstance().call(this, 0, mProfile, true);
    }

    public void onLaunchMessagingUi4(View view) {
        setRemoteGroupProfile(mUser4);
        MesiboUI.launchMessageView(this, null, mRemoteUser.groupid);
        //Log.d(TAG, "Mesibo.isReady()__" + Mesibo.isReady());
    }

    @Override
    public void Mesibo_onConnectionStatus(int status) {
        Log.d(TAG, "Message Status: " + status);
        //mConnStatus.setText("Connection Status: " + status);
    }

    @Override
    public boolean Mesibo_onMessage(Mesibo.MessageParams messageParams, byte[] data) {
        try {
            String message = new String(data, "UTF-8");

            Toast toast = Toast.makeText(getApplicationContext(),
                    message,
                    Toast.LENGTH_SHORT);

            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);

            toast.show();

        } catch (Exception e) {
        }

        return true;
    }

    @Override
    public void Mesibo_onMessageStatus(Mesibo.MessageParams messageParams) {
        Log.d(TAG, "Message Status: " + messageParams.getStatus());
        //mMessageStatus.setText("Message Status: " + messageParams.getStatus());
    }

    @Override
    public void Mesibo_onActivity(Mesibo.MessageParams messageParams, int i) {

        Log.d(TAG, "Mesibo_onActivity: " + i);

    }

    @Override
    public void Mesibo_onLocation(Mesibo.MessageParams messageParams, Mesibo.Location location) {

    }

    @Override
    public void Mesibo_onFile(Mesibo.MessageParams messageParams, Mesibo.FileInfo fileInfo) {

    }

    @Override
    public boolean MesiboCall_onNotify(int i, Mesibo.UserProfile userProfile, boolean b) {
        return false;
    }

    @Override
    public MesiboVideoCallFragment MesiboCall_getVideoCallFragment(Mesibo.UserProfile userProfile) {
        Log.d(TAG, "----MesiboVideoCallFragment---- " );
        return null;
    }
//@Override
//public MesiboVideoCallFragment MesiboCall_getVideoCallFragment(Mesibo.UserProfile userProfile) {
//
//    VideoCallFragment videoCallFragment = new VideoCallFragment();
//   // videoCallFragment.setProfile(userProfile);
//
//    return videoCallFragment;
//}
    @Override
    public MesiboAudioCallFragment MesiboCall_getAudioCallFragment(Mesibo.UserProfile userProfile) {
        return null;
    }

    @Override
    public Fragment MesiboCall_getIncomingAudioCallFragment(Mesibo.UserProfile userProfile) {
        return null;
    }

    public void onLogout(View view) {
        Mesibo.setAccessToken("");
        Mesibo.stop(true);
        Mesibo.reset();
        //Mesibo.resetDB();

        mLoginButton1.setVisibility(View.VISIBLE);
        mLoginButton2.setVisibility(View.VISIBLE);
        mLoginButton3.setVisibility(View.VISIBLE);
        mLogoutButton.setVisibility(View.GONE);
        userLayout1.setVisibility(View.GONE);
        userLayout2.setVisibility(View.GONE);
        userLayout3.setVisibility(View.GONE);
        userLayout4.setVisibility(View.GONE);
    }
}

