import 'package:flutter/material.dart';
//import 'package:getflutter/getflutter.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.amber[800],
        appBar: AppBar(
          title: Text('Sample'),
          backgroundColor: Colors.amber[700],
          elevation: 0.0,
          centerTitle: true,
        ),
        body: Padding(
          padding: EdgeInsets.fromLTRB(30, 40, 0, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: CircleAvatar(
                  backgroundColor: Colors.amber[700],
                  backgroundImage: AssetImage('assets/giphy.gif'),
                  radius: 30.0,
                ),
              ),
              Divider(
                height: 60.0,
                color: Colors.grey[850],
                thickness: 0.5,
                endIndent: 30.0,
              ),
              Text('Name',
                  style: TextStyle(
                    color: Colors.white,
                    letterSpacing: 2.0,
                  )),
              SizedBox(height: 10.0),
              Text(
                'Sample',
                style: TextStyle(
                    color: Colors.white,
                    letterSpacing: 2.0,
                    fontSize: 28.0,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 30.0),
              Text('Power',
                  style: TextStyle(
                    color: Colors.white,
                    letterSpacing: 2.0,
                  )),
              SizedBox(height: 10.0),
              Text(
                '100',
                style: TextStyle(
                    color: Colors.white,
                    letterSpacing: 2.0,
                    fontSize: 28.0,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 30.0),
              Row(
                children: [
                  Icon(Icons.check_circle_outline),
                  SizedBox(width: 10.0),
                  Text('bulabura',
                      style: TextStyle(
                        fontSize: 10.0,
                        letterSpacing: 1.0,
                      )),
                ],
              ),
              Row(
                children: [
                  Icon(Icons.check_circle_outline),
                  SizedBox(width: 10.0),
                  Text('bulabura1',
                      style: TextStyle(
                        fontSize: 10.0,
                        letterSpacing: 1.0,
                      )),
                ],
              ),
              Row(
                children: [
                  Icon(Icons.check_circle_outline),
                  SizedBox(width: 10.0),
                  Text('bulabura2',
                      style: TextStyle(
                        fontSize: 10.0,
                        letterSpacing: 1.0,
                      )),
                ],
              ),
              Center(
                child: SizedBox(
                    height: 150,
                    width: 80,
                    // backgroundColor: Colors.amber[800],
                    child: Image.asset('assets/cat.png')),
                // backgroundColor: Colors.amber[800],
                // backgroundImage: AssetImage('assets/cat.png'),
                // radius: 60.0,
              ),
            ],
          ),
        ));
  }
}
