import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dice.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Dice game',
      home: LogIn(),
    );
  }
}

class LogIn extends StatefulWidget {
  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  TextEditingController controller =TextEditingController();
  TextEditingController controller2 =TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Log in'),
        backgroundColor: Colors.redAccent,
        centerTitle: true,
        leading: IconButton(
            icon: Icon(Icons.menu),
            onPressed: (){}
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.search),
              onPressed: (){}
          )
        ],
      ),
      body: Builder(
        builder: (context) {
          return GestureDetector(
            onTap: (){
              unFocus(context);
              //FocusScope.of(context).unfocus();
              //print(GestureDetector);
            },
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(padding: EdgeInsets.only(top:50)),
                  Center(
                    child: Image(
                      image: AssetImage('images/chef.gif'),
                      width:170,
                      height:190,
                    ),
                  ),
                  Form(child:
                  Theme(data: ThemeData(
                      primaryColor: Colors.teal,
                      inputDecorationTheme: InputDecorationTheme(labelStyle: TextStyle(
                        color:Colors.teal,
                        fontSize: 15.0,
                      ))
                  ),
                    child: Container(
                      padding: EdgeInsets.all(40.0),
                      child: Column(
                        children: [
                          TextField(
                            controller: controller,
                            decoration: InputDecoration(
                              labelText: 'Enter "Dice"',
                            ),
                            keyboardType: TextInputType.emailAddress,
                          ),
                          TextField(
                            controller: controller2,
                            decoration: InputDecoration(
                              labelText: 'Enter Password',
                            ),
                            keyboardType: TextInputType.text,
                            obscureText: true,
                          ),
                          SizedBox(height: 30,),
                          ButtonTheme(
                            minWidth:100.0,
                            height:50.0,
                            child: RaisedButton(
                              child: Icon(Icons.arrow_forward),
                              color: Colors.orangeAccent,
                              onPressed: (){
                                if(controller.text=='dice' && controller2.text=='1234') {
                                  Navigator.push(context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) => Dice()));
                                }
                                else if(controller.text=='dice' && controller2.text!='1234') {
                                  ShowSnackBar2(context);
                                }
                                else if(controller.text!='dice' && controller2.text=='1234') {
                                  ShowSnackBar3(context);
                                }
                                else{
                                  ShowSnackBar(context);
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                  ),
                ],
              ),
            ),
          );
        },
      )
    );
  }
}


void unFocus(BuildContext context){
  FocusScope.of(context).unfocus();
  print(GestureDetector);
}
void ShowSnackBar(BuildContext context){
  Scaffold.of(context).showSnackBar(
      SnackBar(
        content:
          Text('Log in 정보를 확인하세요',
            textAlign: TextAlign.center,),
        duration:Duration(seconds:2),
        backgroundColor:Colors.blue,
      ),
  );
}

void ShowSnackBar2(BuildContext context){
  Scaffold.of(context).showSnackBar(
    SnackBar(
      content:
      Text('비밀번호가 일치하지 않습니다.',
        textAlign: TextAlign.center,),
      duration:Duration(seconds:2),
      backgroundColor:Colors.blue,
    ),
  );
}

void ShowSnackBar3(BuildContext context){
  Scaffold.of(context).showSnackBar(
    SnackBar(
      content:
      Text('Dice의 철자를 확인하세요',
        textAlign: TextAlign.center,),
      duration:Duration(seconds:2),
      backgroundColor:Colors.blue,
    ),
  );
}