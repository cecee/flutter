import 'dart:io';

main(){
  showData();
}
void showData(){
  //print('showData');
  startTask();
  accessData();
  fetchData();
}

void startTask(){
  print('요청수행시작');
}

void accessData(){
  Duration time=Duration(seconds: 3);
  if(time.inSeconds>2){
    //sleep(time);
    Future.delayed(time, (){
      print('데이타처리완료');
    });

  }
  else{
    print('데이타를 가져왔습니다');
  }
}

void fetchData(){
  print('잔액은 8500원입니다');
}
