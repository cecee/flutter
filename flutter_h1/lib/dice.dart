import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:math';
import 'package:fluttertoast/fluttertoast.dart';


class Dice extends StatefulWidget {
  @override
  _DiceState createState() => _DiceState();
}

class _DiceState extends State<Dice> {
  int leftDice=1;
  int rightDice=5;
  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
        appBar: AppBar(

        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(padding: EdgeInsets.all(20.0),
                child: Row(
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // Image(image:AssetImage('images/dice1.png'),
                    //   width:300.0,
                    // ),
                    Expanded(
                     // flex:2,
                        child: Image.asset('images/dice$leftDice.png')),
                    SizedBox(width:30.0),
                    Expanded(
                     // flex:1,
                        child: Image.asset('images/dice$rightDice.png')),
                  ],
                ),
              ),
              ButtonTheme(
                minWidth:200,
                height: 60.0,
                child: RaisedButton(
                        child: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                       size: 60.0,
                  ),
                  elevation: 0.0,
                  color:Colors.grey[300],
                  shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),

                    onPressed: (){
                          setState(() {
                            leftDice=Random().nextInt(6)+1;
                            rightDice=Random().nextInt(6)+1;
                          });
                          showToast('leftDice is $leftDice | rightDice is $rightDice');
                    }),
              ),
            ],
          ),
        ),
      );
    //   Container(
    //   child: Column(children: [
    //     Text('Text Test',
    //       style: TextStyle(
    //         color: Colors.red,
    //         fontSize: 20.0,
    //       ),
    //     ),
    //     Text('Text Test',
    //       style: TextStyle(
    //         color: Colors.red,
    //         fontSize: 20.0,
    //       ),
    //     ),
    //
    //   ],),
    // );
  }
}

void showToast(String msg){
  Fluttertoast.showToast(
    msg: msg,
    backgroundColor:Colors.green,
    textColor:Colors.yellowAccent,
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.BOTTOM,
  );
}
