import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Method Channel Test',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static const MethodChannel _channel =
  const MethodChannel('com.example.method_channel_test');

  String _platformVersion = 'Unknown';
  String _platformString = 'Unknown';
  int _platformInt = -1;

  Future<String> getPlatformVersion() async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
  Future<String> getAndroidString() async {
    final String str = await _channel.invokeMethod('getAndroidString');
    return str;
  }
  Future<int> getAndroidInt() async {
    final int value = await _channel.invokeMethod('getAndroidInt');
    return value;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Method Channel Test"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              child: Text("Get Platform Version"),
              onPressed: () async {
                String result = await getPlatformVersion();
                setState(() {
                  _platformVersion = result;
                });
              },
            ),
            RaisedButton(
              child: Text("Get getAndroidString"),
              onPressed: () async {
                String result = await getAndroidString();
                setState(() {
                  _platformString = result;
                });
              },
            ),
            RaisedButton(
              child: Text("Get getAndroidInt"),
              onPressed: () async {
                int result = await getAndroidInt();
                setState(() {
                  _platformInt = result;
                });
              },
            ),
            Text(_platformVersion),
            Text(_platformString),
            Text('{$_platformInt}'),
          ],
        ),
      ),
    );
  }
}