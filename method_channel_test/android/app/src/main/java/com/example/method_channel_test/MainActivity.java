//package com.example.method_channel_test;
//
//import io.flutter.embedding.android.FlutterActivity;
//
//public class MainActivity extends FlutterActivity {
//}
package com.example.method_channel_test;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;
import java.util.UUID;


public class MainActivity extends FlutterActivity {

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);
        final MethodChannel channel = new MethodChannel(flutterEngine.getDartExecutor(), "com.example.method_channel_test");
        channel.setMethodCallHandler(handler);
    }

    private MethodChannel.MethodCallHandler handler = (methodCall, result) -> {
        Log.d("TAG", "methodCall.method:"+methodCall.method);
        if (methodCall.method.equals("getPlatformVersion")) {
            result.success("Android Version: " + Build.VERSION.RELEASE);
        }
        else if (methodCall.method.equals("getAndroidString")) {
            String id = UUID.randomUUID().toString();
            Log.d("TAG", id);
            result.success("getAndroidString: " + id);
        }
        else if (methodCall.method.equals("getAndroidInt")) {
            int randomNum = (int) (Math.random() * 10);
            result.success(randomNum);
        }

        else {
            result.notImplemented();
        }
    };
}