import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'package:fluttertimerapp2/mywidget2.dart';
import 'package:provider/provider.dart';
import 'counter.dart';

class MyWidget1 extends StatefulWidget {
  @override
  _MyWidget1State createState() => _MyWidget1State();
}

class _MyWidget1State extends State<MyWidget1> {
  var title = 'MyWidget1';
  var timer;

  void initState() {
    SystemChrome.setEnabledSystemUIOverlays([]);
    super.initState();
    {
      debugPrint('Start Timer');
      timer = Timer.periodic(Duration(seconds: 1), (_) {
        setState(() {
          Provider.of<Counter>(context, listen: false).addCount();
        });
      });
    }




    //WidgetsBinding.instance
    //    .addPostFrameCallback((_) => startWatch());
  }

  @override
  Widget build(BuildContext context) {
    var counter = Provider.of<Counter>(context);
    return
     // SafeArea(
     // child:
    Scaffold(
        body: Container(
          color: Color(0x8000A000),
         // child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(top:18.0),
              child: Column(
                  children: <Widget>[
                     Row (
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text('Customer Services',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                            ),

                          ),
                          SizedBox(width:20.0),
                          Text('057-0000-0000',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          SizedBox(width:20.0),
                        ],
                      ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween ,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: 100.0,
                          height:100.0,
                          color: Colors.blue,
                          child: Text('${counter.count}',),
                        ),
                        Container(
                          width:100.0,
                          height:100.0,
                          color: Colors.blue,
                          child: Text('${counter.count}',),
                        ),
                      ],
                    ),



                    //   Container(color: Colors.green),
                    //   Container(color: Colors.green),
                    // ]),


                    //Center(child: Text(title, style: TextStyle(fontSize: 40.0))),
                    //Center(child: Text('Counter: ${counter.count}', style: TextStyle(fontSize: 40.0)),),
                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.center,
                    //   children: <Widget>[
                    //     // IconButton(
                    //     //   icon: Icon(Icons.timer, size: 40.0, color: Colors.white70),
                    //     //   onPressed: () {
                    //     //     // debugPrint('Start Timer');
                    //     //     // timer = Timer.periodic(Duration(seconds: 1), (_) {
                    //     //     //   setState(() {
                    //     //     //     Provider.of<Counter>(context, listen: false).addCount();
                    //     //     //   });
                    //     //     // });
                    //     //   },
                    //     // ),
                    //     // IconButton(
                    //     //   icon: Icon(
                    //     //     Icons.skip_next,
                    //     //     size: 40,
                    //     //     color: Colors.white70,
                    //     //   ),
                    //     //   onPressed: () {
                    //     //     Navigator.push(
                    //     //       context,
                    //     //       MaterialPageRoute(builder: (_) => MyWidget2()),
                    //     //     );
                    //     //   },
                    //     // ),
                    //   ],
                    // )
                  ]),
            ),
          //),
        ),
    //  ),
    );
  }

  @override
  void dispose() {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    super.dispose();
    if(timer!=null){
      debugPrint('cancel timer');
      timer.cancel();
    }
  }
}
