package com.cecee.meetchudemo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.mesibo.api.Mesibo;
import com.mesibo.calls.MesiboVideoCallFragment;

public class VideoCallFragment extends MesiboVideoCallFragment implements Mesibo.CallListener, View.OnTouchListener {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View controlView = inflater.inflate(R.layout.fragment_videocall_custom, container, false);

        //Initialize view and click handlers

        // Add buttons click events.
//        disconnectButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                hangup();
//
//
//            }
//        });

//        cameraSwitchButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                switchCamera();
//            }
//        });

//        videoScalingButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (scalingType == ScalingType.SCALE_ASPECT_FILL) {
//                    videoScalingButton.setBackgroundResource(R.drawable.ic_fullscreen_white_48dp);
//                    scalingType = ScalingType.SCALE_ASPECT_FIT;
//                } else {
//                    videoScalingButton.setBackgroundResource(R.drawable.ic_fullscreen_exit_white_48dp);
//                    scalingType = ScalingType.SCALE_ASPECT_FILL;
//                }
//                ///callEvents.onVideoScalingSwitch(scalingType);
//                scaleVideo(true);
//            }
//        });

//        toggleSpeakerButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                toggleSpeaker();
//                boolean enabled = callEvents.onToggleSpeaker();
//                toggleSpeakerButton.setAlpha(enabled ? 1.0f : 0.3f);
//                callEvents.onToggleSpeaker();
//
//            }
//        });

//        toggleMuteButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                toggleMic();
//                boolean enabled = callEvents.onToggleMic();
//                toggleMuteButton.setAlpha(enabled ? 1.0f : 0.3f);
//                callEvents.onToggleMic();
//
//            }
//        });

//        toggleCameraButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                toggleCamera();
//                boolean enabled = callEvents.onToggleCamera();
//                //setButton(toggleCameraButton, enabled);
//                toggleCameraButton.setAlpha(enabled ? 1.0f : 0.3f);
//                callEvents.onToggleCamera();
//            }
//        });

//        mDeclineViewButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                hangup();
//            }
//        });

//        mAcceptViewButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                answer(true);
//                setDisplayMode();
//            }
//        });

        return controlView;
    }

    @Override
    public void onResume() {
        super.onResume();
        Mesibo.addListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        Mesibo.removeListener(this);
    }

    @SuppressWarnings("deprecation")
//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        callEvents = (OnCallEvents) activity;
//    }

    @Override
    public boolean Mesibo_onCall(long peerid, long callid, Mesibo.UserProfile userProfile, int i) {
        return false;
    }

    @Override
    public boolean Mesibo_onCallStatus(long peerid, long callid, int status, int flags, String desc) {

        return false;
    }

    @Override
    public void Mesibo_onCallServer(int type, String url, String username, String credential) {

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }
}