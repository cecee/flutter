import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: FirstPage(),
    );
  }
}

class FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('First Page'),
      ),
      body:Center(
        child: RaisedButton(
            child: Text('Go to the Sencond Page'),
            onPressed: (){
              Navigator.push(context, MaterialPageRoute(
                  builder: (context)=>SecondPage()));
            })
      )
    );
  }
}

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Second Page'),
        ),
        body:Center(
            child: RaisedButton(
                child: Text('Go to the First Page'),
                onPressed: (){
                  Navigator.pop(ctx);
                })
        )
    );
  }
}
